var superagent = require('superagent');
var expect = require('expect.js');

describe('API', function () {
  it('should say hello ben', function (done) {
    superagent.get('http://localhost:3000/')
      .end(function (err, res) {
        expect(err).to.eql(null);
        expect(res.body.message).to.eql('Hello Ben!');
        done()
      });
  });
});