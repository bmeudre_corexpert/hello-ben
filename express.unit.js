var expect = require('expect.js');
var Name = require('./name');

describe('Name', function () {
  it('should have getName function', function () {
    expect(Name).not.to.be(undefined);
    expect(Name.getName).to.be.a('function');
    expect(Name.getName()).to.eql('Ben');
  });
});