var express = require('express');
var app = express();
var Name = require('./name');

app.set('port', process.env.PORT || 3000);

app.get('/', function (req, res, next) {
  res.json({
    message: 'Hello ' + Name.getName() + '!',
    more: true
  });
});

var server = app.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + server.address().port);
});